import {Routes, Route, Navigate} from "react-router-dom";
import {InicioPage} from "../pages/InicioPage.tsx";
import {ProductosPage} from "../pages/ProductosPage.tsx";
import {UsuariosPage} from "../pages/UsuariosPage.tsx";
import {CategoriasPage} from "../pages/CategoriasPage.tsx";

export const AppRouter = () => {
    return (
        <Routes>
            <Route path="/*" element={<Navigate to='/inicio'/>} />
            <Route path="inicio" element={<InicioPage />} />
            <Route path="productos" element={<ProductosPage />} />
            <Route path="usuarios" element={<UsuariosPage />} />
            <Route path="categorias" element={<CategoriasPage />} />
        </Routes>
    )
}