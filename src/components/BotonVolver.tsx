import {useNavigate} from "react-router-dom";

export const BotonVolver = () => {

    const navigate = useNavigate();

    return (
        <button className="btn btn-danger" onClick={()=>navigate(-1)}>Volver</button>
    )
}