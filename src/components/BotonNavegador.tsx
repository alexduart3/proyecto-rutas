import {useNavigate} from "react-router-dom";

interface Props {
    name: string;
    ruta: string;
}

export const BotonNavegador = ({name, ruta}:Props) => {
    const navigate = useNavigate();

    const navegarADestino = () => {
        navigate(ruta);
    }

    return (
        <button className="btn btn-primary"
            onClick={navegarADestino} style={{marginRight: 10}}>{name}
        </button>
    )
}