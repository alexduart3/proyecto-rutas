import {BotonVolver} from "../components/BotonVolver.tsx";
import {useState} from "react";
import {Usuario} from "../interfaces/Usuario.ts";
import {useForm} from "../hooks/useForm.ts";

const estadoInicialFormulario = {
    nombre: "",
    apellido: ""
}

export const UsuariosPage = () => {
    const [usuarios, setUsuarios] = useState<Usuario[]>([]);
    const {nombre, apellido, resetForm, onChange} = useForm(estadoInicialFormulario);

    const agregarUsuario = (nombre: string, apellido: string) => {
        const nuevoUsuario = {
            id: usuarios.length + 1,
            nombre,
            apellido
        }
        setUsuarios(usuarios.concat(nuevoUsuario));
        resetForm();
    }

    const onAgregarClick = () => {
        if(nombre.length === 0 || apellido.length === 0) {
            alert('Debe ingresar el nombre y el apellido para crear el usuario.');
            return
        }
        agregarUsuario(nombre, apellido);
    }

    return (
        <>
            <div className="container-fluid">
                <h1>Usuarios</h1>
                <hr />

                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" placeholder="Ingrese el nombre" className="form-control"
                                value={nombre} onChange={(event) => {
                                onChange("nombre", event.target.value)
                            }}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" placeholder="Ingrese el apellido" className="form-control"
                                value={apellido} onChange={(event) => {
                                onChange("apellido", event.target.value)
                            }}
                            />
                        </div>
                    </div>
                </div>

                <br/>

                <div className="d-flex justify-content-end">
                    <button className="btn btn-success me-2" onClick={onAgregarClick}>Agregar Usuario</button>
                    <BotonVolver/>
                </div>

                <br/>

                <div className="d-flex justify-content-center">
                    <div className="col-md-12">
                        {
                            usuarios.length > 0 ? (
                                <table className="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th className="text-center">ID</th>
                                        <th className="text-center">Nombre</th>
                                        <th className="text-center">Apellido</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        usuarios.map(usuario => (
                                            <tr key={usuario.id}>
                                                <td className="text-center">{usuario.id}</td>
                                                <td className="text-center">{usuario.nombre}</td>
                                                <td className="text-center">{usuario.apellido}</td>
                                            </tr>
                                        ))
                                    }
                                    </tbody>
                                </table>
                            ) : (
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12 bg-body-tertiary p-5">
                                            <h4 className="text-danger">La lista está sin usuarios.</h4>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </>
    )
}