import {BotonNavegador} from "../components/BotonNavegador.tsx";

export const InicioPage = () => {

    return (
        <>
            <div className="container-fluid">
                <h1>Inicio</h1>
                <hr />
                <BotonNavegador name="Productos" ruta="/productos"/>
                <BotonNavegador name="Usuarios" ruta="/usuarios"/>
                <BotonNavegador name="Categorías" ruta="/categorias"/>
            </div>
        </>
    )
}