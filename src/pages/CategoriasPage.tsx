import {BotonVolver} from "../components/BotonVolver.tsx";
import {useState} from "react";
import {Categoria} from "../interfaces/Categoria.ts";
import {useForm} from "../hooks/useForm.ts";
import {BotonNavegador} from "../components/BotonNavegador.tsx";

const estadoInicialFormulario = {
    nombre: "",
    descripcion: ""
}

export const CategoriasPage = () => {
    const [categorias, setCategorias] = useState<Categoria[]>([]);
    const {nombre, descripcion, resetForm, onChange} = useForm(estadoInicialFormulario);

    const agregarCategoria = (nombre: string, descripcion: string) => {
        const nuevaCategoria = {
            id: categorias.length + 1,
            nombre,
            descripcion
        }
        setCategorias(categorias.concat(nuevaCategoria));
        resetForm();
    }

    const onAgregarClick = () => {
        if (descripcion.length === 0 || nombre.length === 0) {
            alert('Debe ingresar descripción para crear la categoría.');
            return
        }
        agregarCategoria(nombre, descripcion);
    }

    return (
        <>
            <div className="container-fluid">
                <h1>Categorías</h1>
                <hr/>

                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" placeholder="Ingrese el nombre" className="form-control"
                                value={nombre} onChange={(event) => {
                                onChange("nombre", event.target.value)
                            }}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Descripción</label>
                            <input type="text" name="descripcion" placeholder="Ingrese la descripción" className="form-control"
                                value={descripcion} onChange={(event) => {
                                onChange("descripcion", event.target.value)
                            }}
                            />
                        </div>
                    </div>
                </div>

                <br/>

                <div className="d-flex justify-content-end">
                    <button className="btn btn-success me-2" onClick={onAgregarClick}>Agregar Categoría</button>
                    <BotonNavegador name="Productos" ruta="/productos"/>
                    <BotonVolver/>
                </div>

                <br/>

                <div className="d-flex justify-content-center">
                    <div className="col-md-12">
                        {
                            categorias.length > 0 ? (
                                <table className="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th className="text-center">ID</th>
                                        <th className="text-center">Nombre</th>
                                        <th className="text-center">Descripción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        categorias.map(categoria => (
                                            <tr key={categoria.id}>
                                                <td className="text-center">{categoria.id}</td>
                                                <td className="text-center">{categoria.nombre}</td>
                                                <td className="text-center">{categoria.descripcion}</td>
                                            </tr>
                                        ))
                                    }
                                    </tbody>
                                </table>
                            ) : (
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12 bg-body-tertiary p-5">
                                            <h4 className="text-danger">La lista está sin categorías.</h4>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </>
    )
}