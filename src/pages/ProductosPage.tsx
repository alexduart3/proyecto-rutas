import {BotonVolver} from "../components/BotonVolver.tsx";
import {useState} from "react";
import {Producto} from "../interfaces/Producto.ts";
import {useForm} from "../hooks/useForm.ts";

const estadoInicialFormulario = {
    descripcion: "",
    precio: "",
    fecha_de_ingreso: ""
}

export const ProductosPage = () => {
    const [productos, setProductos] = useState<Producto[]>([]);
    // const [descripcion, setDescripcion] = useState("");
    // const [precio, setPrecio] = useState("");
    const {descripcion, precio, fecha_de_ingreso, resetForm, onChange} = useForm(estadoInicialFormulario);


    const agregarProducto = (descripcion: string, precio: number, fecha_de_ingreso: string) => {
        const nuevoProducto: Producto = {
            id: productos.length + 1,
            descripcion,  //Equivalente a descripcion: descripcion
            precio,
            fecha_de_ingreso
        }
        setProductos(productos.concat(nuevoProducto));
        // setProductos([...productos, producto]); // Spread ...
    }

    const onAgregarClick = () => {
        if(descripcion.length === 0 || precio.length === 0 || fecha_de_ingreso.length === 0) {
            alert('Debe ingresar descripción, precio y fecha de ingreso');
            return
        }
        agregarProducto(descripcion, +precio, fecha_de_ingreso);
        resetForm();
    }

    const sumaPrecios = () => {
        let total = 0;
        for (let i = 0; i < productos.length; i++) {
            total += productos[i].precio;
        }
        return total;
    }

    return (
        <>
            <div className="container-fluid">
                <h1>Productos</h1>
                <hr/>

                <div className="row">
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Descripción</label>
                            <input type="text" name="descripcion" placeholder="Ingrese la descripción"
                                   className="form-control"
                                   value={descripcion} onChange={(event) => {
                                   onChange("descripcion", event.target.value)
                            }}
                            />
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Precio</label>
                            <input type="number" name="precio" placeholder="Ingrese el precio" className="form-control"
                                   value={precio} onChange={(event) => {
                                   onChange("precio", event.target.value)
                            }}
                            />
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Fecha de ingreso</label>
                            <input type="date" name="fecha_de_ingreso" className="form-control"
                                   value={fecha_de_ingreso.split('/').reverse().join('-')} onChange={(event) => {
                                   const date = new Date(event.target.value);
                                   // console.log(date.toUTCString());
                                   onChange("fecha_de_ingreso", `${date.getUTCDate().toString().padStart(2, '0')}/${(date.getUTCMonth() + 1).toString().padStart(2, '0')}/${date.getUTCFullYear()}`)
                            }}
                            />
                        </div>
                    </div>
                </div>

                <br/>

                <div className="d-flex justify-content-end">
                    <button className="btn btn-success me-2" onClick={onAgregarClick}>Agregar Producto</button>
                    <BotonVolver/>
                </div>

                <br/>

                <div className="d-flex justify-content-center">
                    <div className="col-md-12">
                        {
                            productos.length > 0 ? (
                                <>
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th className="text-center">ID</th>
                                            <th className="text-center">Descripción</th>
                                            <th className="text-center">Precio</th>
                                            <th className="text-center">Fecha de Ingreso</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            productos.map(producto => (
                                                <tr key={producto.id}>
                                                    <td className="text-center">{producto.id}</td>
                                                    <td className="text-center">{producto.descripcion}</td>
                                                    <td className="text-end">{producto.precio.toFixed(2).replace('.', ',') + ' Gs.'}</td>
                                                    <td className="text-center">{producto.fecha_de_ingreso}</td>
                                                </tr>
                                            ))
                                        }
                                        <tr>
                                            <td colSpan={2} className="text-center"><b>Total de precios:</b></td>
                                            <td colSpan={2} className="text-end">
                                                {sumaPrecios().toFixed(2).replace('.', ',') + ' Gs.'}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p className="mt-2">Total de registros: <b>{productos.length}</b></p>
                                </>
                            ) : (
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-md-12 bg-body-tertiary p-5">
                                            <h4 className="text-danger">La lista está sin productos.</h4>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

/*
const formatearFecha = (fecha: string) : string => {
    return (fecha.split('-').reverse().join('/'))
}*/