import {useState} from "react";

export const useForm = <T extends object> (estadoInicialFormulario: T) => {

    const [formulario, setFormulario] = useState(estadoInicialFormulario);

    const onChange = <K extends object> (campo: keyof T, valor: keyof K) => {
        setFormulario({
            ...formulario, [campo]: valor
        })
    }

    const resetForm = () => {
        setFormulario(estadoInicialFormulario)
    }

    return {...formulario, formulario, resetForm, onChange}
}