import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {AppRouter} from "./router/AppRouter.tsx";
import {BrowserRouter} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <BrowserRouter>
        <AppRouter />
    </BrowserRouter>
  </React.StrictMode>,
)
